package com.qless.testapi.service;

import com.qless.testapi.Model.RequiredAppleBudget;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

/**
 * @author Walter Finkbeiner
 */
@Service
public class RequiredAppleBudgetService {

    public RequiredAppleBudget getRequiredAppleBudget(LocalDate startDate, Short days) {
        Double result = 0.0;
        LocalDate endDate = startDate.plusDays(days);
        LocalDate calculatedDate = startDate;
        while (calculatedDate.isBefore(endDate)) {
            if (calculatedDate.getDayOfMonth() <= 7) {
                result += 0.05;
            } else if (calculatedDate.getDayOfMonth() <= 14) {
                result += 0.07;
            } else if (calculatedDate.getDayOfMonth() <= 21) {
                result += 0.08;
            } else {
                result += 0.04;
            }
            calculatedDate = calculatedDate.plusDays(1);
        }
        return new RequiredAppleBudget(Math.round(result * 100.0) / 100.00);
    }


}
