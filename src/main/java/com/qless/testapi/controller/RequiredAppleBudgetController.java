package com.qless.testapi.controller;

import com.qless.testapi.Model.CustomError;
import com.qless.testapi.Model.RequiredAppleBudget;
import com.qless.testapi.service.RequiredAppleBudgetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import javax.validation.ConstraintViolationException;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * @author Walter Finkbeiner
 */
@RestController
@Validated
@RequestMapping("/requiredAppleBudget")
public class RequiredAppleBudgetController {

    @Autowired
    private RequiredAppleBudgetService requiredAppleBudgetService;

    @GetMapping
    public @ResponseBody
    RequiredAppleBudget getRequiredAppleBudget(@RequestParam @NotNull @DateTimeFormat(pattern = "yyyyMMdd") LocalDate date,
                                               @RequestParam @NotNull @Min(value = 1, message = "'days' must be greater than 0") @Max(value = 365, message = "'days' must be equal or lower than 365") Short days) {
        return requiredAppleBudgetService.getRequiredAppleBudget(date, days);
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler({ConstraintViolationException.class})
    public CustomError handleConstraintViolation(
            ConstraintViolationException ex) {
        String error = ex.getConstraintViolations().iterator().next().getMessage();
        return new CustomError(error);
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public CustomError handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex) {
        String error = String.format("'%s' is not a valid value for '%s'", ex.getValue(), ex.getName());
        CustomError customError = new CustomError(error);
        return customError;
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public CustomError handleMissingServletRequestParameterException(MissingServletRequestParameterException ex) {
        String error = String.format("Required parameter '%s' is not present", ex.getParameterName());
        CustomError customError = new CustomError(error);
        return customError;
    }
}