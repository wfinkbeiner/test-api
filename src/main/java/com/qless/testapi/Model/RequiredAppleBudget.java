package com.qless.testapi.Model;

/**
 * @author Walter Finkbeiner
 */
public class RequiredAppleBudget {

    private Double budget;

    public RequiredAppleBudget(Double budget) {
        this.budget = budget;
    }

    public Double getBudget() {
        return budget;
    }

    public void setBudget(Double budget) {
        this.budget = budget;
    }
}
