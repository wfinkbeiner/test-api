package com.qless.testapi.Model;

/**
 * @author Walter Finkbeiner
 */
public class CustomError {
    private String error;

    public CustomError(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
