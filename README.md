# Test API

### About the API
This API that calculates how much budget is required to purchase an Apple for a set number of days:
- Days 1-7 of a given month, the apple costs 0.05 per day. 
- On Days 8-14 the apple costs 0.07 per day,  
- On days 15-21 the apple is at 0.08 per day. 
- All other days the apple is at 0.04 per day.

#### Query parameters
The API accepts 2 parameters, date and days.  
- `date` is the start date in format of yyyyMMdd. 
- `days` is the number of days as an integer to budget for.

#### Rules
- The max number of `days` the API accepts is 365
- The API only accepts valid `date` in the calendar
- Any incorrect date or days and the API returns a 400 error code.

## Play with the API on Heroku:
https://aqueous-tor-48262.herokuapp.com/requiredAppleBudget?date=20181010&days=1

## Play with the API locally:

#### Pre-requisites:

- Git
- Java Development Kit
- Apache Maven

Move to the folder in which you want to clone the repo
```sh
$ cd myFolder
```
Clone the repo:
```sh
$ git clone git@gitlab.com:wfinkbeiner/test-api.git
```

Move to the test-api directory:
```sh
$ cd test-api
```

Execute mvn spring-boot:run command:
```sh
$ mvn spring-boot:run
```
The application will run by default on port 8080:
http://localhost:8080/requiredAppleBudget/?date=20180501&days=7




